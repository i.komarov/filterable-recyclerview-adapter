package net.styleru.i_komarov.filterablerecyclerview.library;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.subjects.PublishSubject;
import rx.subscriptions.Subscriptions;

/**
 * Created by i_komarov on 28.10.16.
 */

public abstract class AbstractFilterableAdapter<T> extends AbstractRecyclerViewAdapter<T> {

    protected PublishSubject<String> filterObservable;

    protected List<T> allItems;
    protected List<T> visibleItems;

    protected Subscription subscription = Subscriptions.empty();


    protected AbstractFilterableAdapter(Class<? extends AbstractViewHolder> vhClass, int itemRes) {
        super(vhClass, itemRes);
        allItems = new ArrayList<>();
        visibleItems = new ArrayList<>();
    }

    protected AbstractFilterableAdapter(Class<? extends AbstractViewHolder> vhClass, int itemRes, PublishSubject<String> filterObservable) {
        super(vhClass, itemRes);
        allItems = new ArrayList<>();
        visibleItems = new ArrayList<>();
        this.filterObservable = filterObservable;
        init();
    }

    public void setItems(List<T> items) {
        this.allItems.addAll(items);
        this.visibleItems.addAll(items);
        notifyDataSetChanged();
    }

    public void setFilterObservable(PublishSubject<String> filterObservable) {
        this.filterObservable = filterObservable;
        init();
    }

    protected void setVisibleItems(List<T> items) {
        this.visibleItems.clear();
        this.visibleItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(AbstractViewHolder<T, AbstractViewHolder.OnItemClickListener<T>> holder, int position) {
        holder.bind(position, visibleItems.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return visibleItems.size();
    }

    protected void init() {
        subscription = filterObservable.subscribe(
                this::filter,
                Throwable::printStackTrace
        );
    }

    protected abstract void filter(String queryString);
}
