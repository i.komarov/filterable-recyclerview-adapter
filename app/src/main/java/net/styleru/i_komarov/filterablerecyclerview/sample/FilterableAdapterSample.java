package net.styleru.i_komarov.filterablerecyclerview.sample;

import net.styleru.i_komarov.filterablerecyclerview.library.AbstractFilterableAdapter;
import net.styleru.i_komarov.filterablerecyclerview.library.AbstractViewHolder;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * Created by i_komarov on 28.10.16.
 */

public class FilterableAdapterSample extends AbstractFilterableAdapter<String> {

    protected FilterableAdapterSample(Class<? extends AbstractViewHolder> vhClass, int itemRes) {
        super(vhClass, itemRes);
    }

    protected FilterableAdapterSample(Class<? extends AbstractViewHolder> vhClass, int itemRes, PublishSubject<String> filterObservable) {
        super(vhClass, itemRes, filterObservable);
    }

    @Override
    protected void filter(String queryString) {
        Observable.just(allItems)
                .subscribeOn(Schedulers.computation())
                //as an alternative, you may put whatever filter you like. This is as much customisable as your imagination is wide
                //for example, you may put your custom mapper with Func2<List<String>, List<String>> implementation, or obj-obj, etc
                .concatMap(Observable::from)
                .filter(item -> item.contains(queryString))
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .subscribe(
                        this::setVisibleItems,
                        Throwable::printStackTrace
                );
    }
}
