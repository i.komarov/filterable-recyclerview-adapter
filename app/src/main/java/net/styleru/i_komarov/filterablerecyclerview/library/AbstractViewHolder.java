package net.styleru.i_komarov.filterablerecyclerview.library;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by i_komarov on 28.10.16.
 */

public abstract class AbstractViewHolder<T, I extends AbstractViewHolder.OnItemClickListener<T>> extends RecyclerView.ViewHolder{

    public AbstractViewHolder(View itemView) {
        super(itemView);
    }

    protected void bind(int position, T item, OnItemClickListener<T> listener) {
        if(listener != null) {
            this.itemView.setOnClickListener(v -> listener.call(item));
        }
    }

    public interface OnItemClickListener<I> {

        void call(I item);
    }
}
