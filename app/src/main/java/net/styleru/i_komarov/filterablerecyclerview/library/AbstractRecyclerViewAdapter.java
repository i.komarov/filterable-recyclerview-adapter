package net.styleru.i_komarov.filterablerecyclerview.library;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by i_komarov on 28.10.16.
 */

public abstract class AbstractRecyclerViewAdapter<T> extends RecyclerView.Adapter<AbstractViewHolder<T, AbstractViewHolder.OnItemClickListener<T>>> {

    @LayoutRes
    private final int itemRes;
    private final Class<? extends AbstractViewHolder> vhClass;

    protected AbstractViewHolder.OnItemClickListener<T> listener;

    protected AbstractRecyclerViewAdapter(Class<? extends AbstractViewHolder> vhClass, int itemRes) {
        this.vhClass = vhClass;
        this.itemRes = itemRes;
    }

    @Override
    public AbstractViewHolder<T, AbstractViewHolder.OnItemClickListener<T>> onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            return vhClass.getConstructor(View.class)
                    .newInstance(
                            LayoutInflater.from(parent.getContext()).inflate(itemRes, parent, false)
                    );
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected void setOnItemClickListener(AbstractViewHolder.OnItemClickListener<T> listener) {
        this.listener = listener;
    }
}
